﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebProgramlamaSite.Startup))]
namespace WebProgramlamaSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
